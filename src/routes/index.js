import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

// const ifNotAuthenticated = (to, from, next) => {
//   if (!store.getters.isAuthenticated) {
//     next();
//     return;
//   }
//   next("/dashboard");
// };

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      component: () => import('./../pages/Home'),
     // beforeEnter: ifNotAuthenticated
    },
    {
      path: '/:ItemName',
      name: 'item',
      component: () => import('./../components/home/components/tabs/TabItenDetail'),
    }
  ]
});